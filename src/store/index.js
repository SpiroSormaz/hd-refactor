import { createStore } from 'vuex'

export default createStore({
  state: {
    user: null,
    tickets:null,
    ticketDetails: null,
    ticketComments: null,
    documentPreview:{
       documentModal: false,
       documentSrc: '',
       documentName:''
    }
  },
  mutations: {
    user(state, user) {
      state.user = user;
    },
    tickets(state, tickets){
      state.tickets = tickets;
    },
    ticketDetails(state, ticketDetails){
      state.ticketDetails = ticketDetails;
    },
    ticketComments(state, ticketComments){
      state.ticketComments = ticketComments;
    },
   
    setDocumentPreview(state, payload) {
      state.documentPreview.documentModal = payload.documentModal;
      state.documentPreview.documentSrc = payload.documentSrc;
      state.documentPreview.documentName = payload.documentName;
    },
  },
  getters:{
    user: (state) => {
      return state.user;
    },
    tickets: (state) => {
      return state.tickets;
    },
    ticketDetails: (state) => {
      return state.ticketDetails
    },
    ticketComments: (state) =>{
      return state.ticketComments
    },
    documentPreview : (state) =>{
      return state.documentPreview
    },
  },
  actions: {
    user(context, user){
      context.commit('user', user);
    },
    tickets(context, tickets){
      context.commit('tickets', tickets);
    },
    async ticketDetails(context, ticketDetails){
      context.commit('ticketDetails', await ticketDetails)
    },
    ticketComments(context, ticketComments){
      context.commit('ticketComments', ticketComments)
    }
  },
  modules: {
  }
})
