const globalPrefix = 'http://localhost:3000/'

const api = {
    login: globalPrefix + 'auth/user/login',
    tickets: globalPrefix + 'api/task/',
    comments: globalPrefix + 'api/comment/',
    documentsPath: globalPrefix + 'assets/task_document/',
    

}

export default api;